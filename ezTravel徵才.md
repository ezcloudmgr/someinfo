##ezTravel易遊網為全國最大線上旅遊網站，提供顧客全方位旅遊商品服務，便利地線上訂位及付款，為國內線上旅遊的領先者。我們認為企業成功最主要的關鍵在於提供能滿足顧客需要的商品及服務，使顧客擁有美好的消費經驗，以及對內給予員工最佳的工作環境。 

###行動應用軟體工程師
#####我們正在找尋對 mobile app 的開發有熱情、天分的工程師與我們一同在競爭激烈的市場內打造全新的使用者體驗。如果你熱愛挑戰，歡迎加入ezTravel的技術團隊。在ezTravel擔任mobile app工程師，你將與ezTravel的團隊一同打造線上線下無疆界的最佳旅遊平台，提供顧客最優質的旅遊服務及美好的旅遊經驗。

#####職務要求:
> 1.Android or iOS 原生app開發。
>
> 2.負責參與ezTravel行動平台的討論、設計及規劃。
>
> 3.熟悉Objective-C 或 Android SDK等 mobile app 開發工具。
>
> 4.熟悉Android 或 iOS 平台 Design guideline。

#####其它
> 1.具有獨立思考、善溝通、自動自發、積極主動，自我管理，喜歡面對不同的挑戰。
>
> 2.熟悉App上架流程為加分項目。
>
> 3.具備相關Server side開發經驗為加分項目。

###Server Side 軟體工程師
#####在ezTravel擔任J2EE軟體工程師的你將面對無數的技術挑戰，例如演算法調整、架構設計、效能優化、安全考量等各種層面的難題。如果Java是你的Native Language，我們歡迎你一同加入ezTravel的技術團隊，打造線上線下無疆界的最佳旅遊平台，提供顧客最優質的旅遊服務及美好的旅遊經驗。

#####職務要求:
> 1.伺服器端系統分析設計、開發及測試。
>
> 2.瞭解RESTful Web Service / SOAP / JMX。
>
> 3.熟悉Service side Java開發生態及相關Framework/Library。
>
> 4.熟悉CVS / SVN / Git的Version Control使用觀念。
>
> 5.熟悉MVC 架構開發。
>
> 6.習慣閱讀英文技術文件。
>
> 7.熟悉JBOSS EAP / Tomcat 。
>
> 8.熟悉Oracle / MySQL等RDBMS。

#####其它
> 1.具有獨立思考、善溝通、自動自發、積極主動，自我管理等個性，喜歡面對不同的挑戰者尤佳。
>
> 2.除Java外具備其它程式語言(Ex: Python / Ruby / JavaScript)開發經驗者尤佳。
>
> 3.具備AWS相關使用經驗為加分項目。
>
> 4.具備Linux(RHEL/Ubuntu Server)環境設定、優化、調校等相關經驗為加分項目。
>
> 5.具備Apache / Nginx調校經驗為加分項目。
>
> 6.具備NoSQL(Ex: Redis/MangoDB/Cassandra等)相關開發經驗為加分項目。
>
> 7.熟悉WEB前端技術及Framework(Ex: jQuery/Bootstrap等)為加分項目。